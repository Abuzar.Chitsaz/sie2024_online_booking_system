import odoorpc

# Paramètres de connexion
url = 'localhost'  
db = 'SIE_2024'
username = 'abu@cui.ch'
password = '100'

# Connexion au serveur Odoo
odoo = odoorpc.ODOO(url, port=8069)  
odoo.login(db, username, password)

# Préparation des données du contact
contact_data = {
    'name': 'Abuzzz222',
    'phone': '+33102030405', 
    # Ajoutez ici d'autres champs si nécessaire
}

# Création du contact
contact_id = odoo.env['res.partner'].create(contact_data)

print(f"Contact créé avec l'ID : {contact_id}")
